import pandas as pd
import numpy as np
import random
import os
import pickle

NUM_STUDENT = 87
NUM_GROUPS = 15

if not os.path.isfile('raffle_numbers.npy'):
    raffle_numbers = list(range(1,NUM_STUDENT+1))
    groups = np.array_split(raffle_numbers,NUM_GROUPS)
    print(f'It is the first time we run the game, generate {len(raffle_numbers)} players to start')
    group_results= {}
else:
    raffle_numbers = list(np.load('raffle_numbers.npy'))
    # group_results = pd.read_csv('current_groups.csv').to_dict('list')
    with open("current_groups.pkl", "rb") as tf:
        group_results = pickle.load(tf)
    print(f'We have started the game before, the number of remianing seats is {len(raffle_numbers)}.')

if len(raffle_numbers)==0:
    print('reach the end of grouping!')
    quit(0)

def get_group(player):
    players = list(range(1,NUM_STUDENT+1))
    groups = np.array_split(players,NUM_GROUPS)
    for i,group in enumerate(groups):
        if player in group:
            return i+1



new_player = random.randint(1, NUM_STUDENT)
while new_player not in raffle_numbers:
    new_player = random.randint(1, NUM_STUDENT)

raffle_numbers.remove(new_player)
np.save('raffle_numbers.npy',raffle_numbers)

group_num = get_group(new_player)
if f'Group {group_num}' in group_results.keys():
    group_memebers = group_results[f'Group {group_num}']
    group_memebers.append(new_player)
    group_results[f'Group {group_num}'] = group_memebers
else:
    group_results[f'Group {group_num}']=[new_player]

print('Player ', new_player, ' in Group ',group_num)
print(group_results)

with open("current_groups.pkl", "wb") as handle:
    pickle.dump(group_results, handle, protocol=pickle.HIGHEST_PROTOCOL)
# pd.DataFrame(group_results).to_csv('current_groups.csv',index=False)
